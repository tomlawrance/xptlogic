#version 300 es
precision mediump float;

uniform sampler2D image;
uniform vec3 tintColour;

out vec4 FragColor;
in vec2 textureCoords;

void main()
{
    vec4 sampled = texture(image,textureCoords);
    vec4 tint = vec4(tintColour, sampled.x);
    FragColor = tint;
}