#version 300 es
layout (location = 0) in vec4 aPos;

uniform mat4 ProjectionMatrix;
uniform mat4 ModelMatrix;
out vec2 textureCoords;
void main()
{
    gl_Position = ProjectionMatrix * ModelMatrix * vec4(aPos.xy, 0.0, 1.0);
    textureCoords = aPos.zw;
}