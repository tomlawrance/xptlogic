#version 300 es
precision mediump float;

uniform sampler2D image;

out vec4 FragColor;
in vec2 textureCoords;

void main()
{
    FragColor =  texture(image,textureCoords);
}