//
// Created by toml on 23/04/2019.
//

#include <Engine/Engine.h>
#include <Engine/Managers/TextFileHelper.h>
#include <GLES3/gl3.h>
#include "GamePlayScene.h"
enum eProcessActions { LOAD_NEXT = 1, RELOAD_CURRENT = 2, START = 3};

void GamePlayScene::processEvent(int eventAction) {
    Scene::processEvent(eventAction);

    if(eventAction == LOAD_NEXT)
        loadNextLevel();
    else if(eventAction == RELOAD_CURRENT)
        loadCurrentLevel();
    else if(eventAction == START)
        loadCurrentLevel();
}

void GamePlayScene::load(const char *sceneName) {
    Scene::load(sceneName);

    loadNextLevel();

    btnNextLevel = new Label();
    btnNextLevel->setFont(Engine::getEngineInstance()->getFontManager()->getFont("font.fnt"));
    btnNextLevel->mHorizontalPosition = IRenderable::Center;
    btnNextLevel->mVerticalPosition = IRenderable::Absolute;
    btnNextLevel->setOnClickCallback(&Scene::processEvent, this,1);
    btnNextLevel->setText("Next Level");

    btnRetryLevel = new Label();
    btnRetryLevel->setOnClickCallback(&Scene::processEvent, this,2);
    btnRetryLevel->setFont(Engine::getEngineInstance()->getFontManager()->getFont("font.fnt"));
    btnRetryLevel->mHorizontalPosition = IRenderable::Center;
    btnRetryLevel->mVerticalPosition = IRenderable::Absolute;
    btnRetryLevel->setText("Retry Level");


    lbHitScore= new Label();
    lbHitScore->setFont(Engine::getEngineInstance()->getFontManager()->getFont("game_font.fnt"));
    lbHitScore->setText("0/0");
    lbHitScore->setColour(109,255,12);
    addItem(new SceneItem(lbHitScore));

    lbSecondsLeft= new Label();
    lbSecondsLeft->setLocation(0, Engine::getEngineInstance()->getRenderer()->getHeight()-80);
    lbSecondsLeft->setFont(Engine::getEngineInstance()->getFontManager()->getFont("game_font.fnt"));
    lbSecondsLeft->setText("0.0");
    lbSecondsLeft->setColour(109,255,12);
    addItem(new SceneItem(lbSecondsLeft));

    mDialog = new DialogWindow("window.png");
    mDialog->mVerticalPosition = IRenderable::ePosition::Center;
}
void GamePlayScene::showDialog(){
    //build dialog.
     mSceneState = eSceneState::DIALOG_SHOWING;
    mDialog->clearControls();
    if(mCurrentLevelData->mLevelPassed == true) {
        mDialog->setTitle("Level Complete!");
        mDialog->addControl(btnNextLevel);
    }
    else{
        mDialog->setTitle("Try Again!");
        mDialog->addControl(btnRetryLevel);
    }
};
void GamePlayScene::showLoadTip() {
    if(mLoadTipDialog== NULL){
        mLoadTipDialog = new DialogWindow("window.png");
        mLoadTipDialog->setTitle("Get Ready!");

        Sprite* padSprite = new Sprite();
        padSprite->setTexture(mPadTexture);
        padSprite->setColour(mCurrentLevelData->mPrimaryColour[1],mCurrentLevelData->mPrimaryColour[2],mCurrentLevelData->mPrimaryColour[3]);
        padSprite->mHorizontalPosition = IRenderable::ePosition::Center;
        mLoadTipDialog->addControl(padSprite);


    }
}

void GamePlayScene::update(float deltaTime) {
    if(mSceneState == eSceneState::READY) {
        Scene::update(deltaTime);

        float totalHeight = mScreenHeight;
        mRunningTime += deltaTime;

        float timeLeft = mCurrentLevelData->mTimeToComplete - mRunningTime;
        char strTimeLeft[10];
        sprintf(strTimeLeft, "%.2f", timeLeft);
        lbSecondsLeft->setText(strTimeLeft);
        lbSecondsLeft->setLocation( Engine::getEngineInstance()->getRenderer()->getWidth() - lbSecondsLeft->getBounds()->width,0);

        if (mRunningTime >= mCurrentLevelData->mTimeToComplete) {
            showDialog();
        }


        for(int i=0; i < mPads.size();i++) {
            Pad* p= mPads[i];
            p->IsEnabled = true;// ( mRunningTime > p->mSpawnTime);
            if(p->IsEnabled){
            if(p->mDirection == Pad::eDirection::Down){

                //float pixelsToMovePerSecond = ((mCurrentLevelData->mTotalPads * mPadWidth) / mCurrentLevelData->mTimeToComplete ) + (p->mColoumn * 0.3f) ;

                p->CurrentY += ( mCurrentLevelData->mPixelMoveSpeed * deltaTime );

                if(p->CurrentY > totalHeight)
                    p->CurrentY = getMinOffscreenPad();//  -mPadSprite->getBounds()->height;
            }
            }
        }
    }
}

void GamePlayScene::input(int action, float x, float y) {
    Scene::input(action, x, y);
    //check pad hit.
    if(mSceneState == eSceneState::DIALOG_SHOWING){
        if(mDialog) {
            mDialog->checkInput(x, y);
            return;
        }
    }
    if(action == InputManager::Down){
        for(int i=0; i < mPads.size();i++){
            Pad* p = mPads[i];
            if(!p->IsHit) {
                if (y > p->CurrentY && y < p->CurrentY + mPadSprite->getBounds()->height)
                    if (x > p->CurrentX && x < p->CurrentX + mPadSprite->getBounds()->width)
                        //hit
                    {
                        if(!p->IsDud) {
                            p->IsHit = true;
                            mCurrentLevelData->mCurrentHits++;
                            checkScores();
                            break;
                        }
                        else {
                            loadCurrentLevel();
                        }
                        //todo some scoring.
                    }
            }
        }
    }
}

void GamePlayScene::render(Renderer *renderer, float dt) {
    Scene::render(renderer, dt);

        for (int i = 0; i < mPads.size(); i++) {
            Pad *p = mPads[i];

            if (p->IsEnabled && !p->IsHit) {
                mPadSprite->setLocation(p->CurrentX, p->CurrentY);
                if(p->IsDud)
                    mPadSprite->setColour(mCurrentLevelData->mDudColour[1],mCurrentLevelData->mDudColour[2],mCurrentLevelData->mDudColour[3]);
                else
                    mPadSprite->setColour(mCurrentLevelData->mPrimaryColour[1],mCurrentLevelData->mPrimaryColour[2],mCurrentLevelData->mPrimaryColour[3]);
                mPadSprite->render(renderer);
            }
        }
   if(mSceneState == eSceneState::DIALOG_SHOWING){
       if(mDialog != NULL){
           float height =  renderer->getHeight() * 0.5f;
           mDialog->setSize(renderer->getWidth() - 40, height);
           mDialog->setLocation(20,height - (height * 0.5f) );
           mDialog->render(renderer);


       }
   }
}

void GamePlayScene::activate() {
    mRunningTime = 0;

}

void GamePlayScene::setup(Renderer *renderer) {
    srand(time(0));
    mPadTexture = Engine::getEngineInstance()->getAssetManager()->LoadTexture(mCurrentLevelData->mPadTextureFile.c_str());
    mPadTexture->Upload();
    mPadSprite = new Sprite();
    mPadSprite->setTexture(mPadTexture);
    mPadSprite->setColour(1,0,0);
    mPadSprite->setShader(ShaderProgram::getShaderProgramFromCache("sprite.vs","sprite_tint.fs"));

    mPadWidth = renderer->getWidth() / 3;
    mPadSprite->setSize(mPadWidth,mPadWidth);

    generatePads();

    setSceneState(eSceneState::READY);
    mScreenHeight = renderer->getHeight();
}

void GamePlayScene::checkScores() {
    if (mCurrentLevelData->mCurrentHits == mCurrentLevelData->mTarget) {
        mCurrentLevelData->mLevelPassed = true;
        showDialog();
    }
    if(lbHitScore != NULL) {
        char score[10];
        sprintf(score, "%i/%i", mCurrentLevelData->mCurrentHits, mCurrentLevelData->mTarget);
        lbHitScore->setText(score);
    }

}

void GamePlayScene::loadNextLevel() {


    mCurrentLevelIndex++;
    if(mCurrentLevelIndex > mMaxLevels) {
        mCurrentLevelIndex = 1;
    }
    loadLevelData(mCurrentLevelIndex);

}

void GamePlayScene::loadLevelData(int mLevel) {


    mCurrentLevelData = new LevelData();
    char levelFile[100];
    sprintf(levelFile,"level%d.lvl",mLevel);
    mCurrentLevelData->loadLevel(levelFile);



    mRunningTime = 0;
    generatePads();
    mSceneState= eSceneState ::READY;


}

void GamePlayScene::generatePads() {

    std::map<std::string,bool> padLocations;
    std::map<int,bool> padDuds;
    mPads.clear();

    for(int i= mPads.size()-1; i >= 0 ; i--){
        Pad* p = mPads[i];
        delete p;
    }
    int rows = mCurrentLevelData->mTotalPads;
    int duds = mCurrentLevelData->mTotalPads - mCurrentLevelData->mTarget;

    for(int i=0;i < duds; i++){
        int di = rand() % mCurrentLevelData->mTotalPads;
        if(padDuds[di] == false) {
            padDuds[di] = true;
        }

    }
    for(int i=0; i < mCurrentLevelData->mTotalPads;i++){
        Pad* p = new Pad(5);

        p->mDirection = Pad::eDirection::Down;
        bool gotFreeSlot = false;

        while(!gotFreeSlot){
            int row = rand() % rows;
            int col = rand() % 3;

            char hash[10];
            sprintf(hash,"%d%s%d", col,"|", row);
            std::string rowCol = std::string(hash);
            if(padLocations[rowCol] == false){
                p->mRow = -row;
                p->mColoumn = col;
                padLocations[rowCol] = true;
                gotFreeSlot = true;
                break;
            }
        }




        p->IsDud = (padDuds[i] == true);
        p->mSpawnTime =   rand() % (((int)mCurrentLevelData->mTimeToComplete)- 5) +1;



        p->CurrentX = (p->mColoumn * mPadWidth);
        p->CurrentY = p->mRow * mPadWidth;
        p->IsEnabled = false;
        p->IsHit = false;
        mPads.push_back(p);

    }
    mSpawnDelay = mCurrentLevelData->mTotalPads / mCurrentLevelData->mTimeToComplete;
    float totalNonDudPadHeight = getNonDudTotalHeight();
    mCurrentLevelData->mPixelMoveSpeed = ((totalNonDudPadHeight) / mCurrentLevelData->mTimeToComplete );

}

void GamePlayScene::loadCurrentLevel() {
    loadLevelData(mCurrentLevelIndex);//restart;
    checkScores();
}




LevelData::LevelData() {
    mTotalPads = 0;
    mMaxDropSpeed = 0;
    mTimeToComplete = 0;
    mLevelPassed = false;
}

void LevelData::loadLevel(const char *lvlFile) {
    const char* lvlContent= Engine::getEngineInstance()->LoadAsset(lvlFile);
    std::vector<KVPair> kv = Scene::getContentPairs(lvlContent);
    for(int i=0; i < kv.size();i++){
        KVPair pair = kv[i];
        if(strcmp(pair.key.c_str(), "totalpads") == 0){
            mTotalPads = atoi(pair.value.c_str());
        }
        else if(strcmp(pair.key.c_str(), "timetocomplete") == 0) {
            mTimeToComplete = atoi(pair.value.c_str());
        }
        else if(strcmp(pair.key.c_str(), "target") == 0) {
           mTarget = atoi(pair.value.c_str());
        }
        else if(strcmp(pair.key.c_str(), "padtexture") == 0) {
            mPadTextureFile = std::string(pair.value);
        }
        else if(strcmp(pair.key.c_str(), "padprimarycolour") == 0) {

            TextHelper::colourFromString(pair.value.c_str(), mPrimaryColour);
        }
        else if(strcmp(pair.key.c_str(), "paddudcolour") == 0) {
            TextHelper::colourFromString(pair.value.c_str(), mDudColour);
        }
    }

    mCurrentHits = 0;
    mLevelPassed = false;
}

Pad::Pad(float lifeTime) {
    mLifeTime = lifeTime;
    mActive = false;
    IsDud = false;
}
