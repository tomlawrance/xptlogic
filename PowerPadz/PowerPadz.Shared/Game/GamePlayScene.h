//
// Created by toml on 23/04/2019.
//

#ifndef POWERPADZ_GAMEPLAYSCENE_H
#define POWERPADZ_GAMEPLAYSCENE_H


#include <Engine/Scene/Scene.h>
#include <Engine/Scene/Objects/TwoD/DialogWindow.h>
#include <Engine/Scene/Objects/TwoD/Label.h>

class Pad{
public:
    enum eDirection { Down = 0, Right = 1, Up = 2, Left = 3};
    Pad(float lifeTime);

    int mColoumn;
    int mRow;
    eDirection mDirection;
    float mLifeTime;
    int mColour; //index into a pallete
    bool mActive;
    float mSpawnTime ;
    float CurrentX;
    float CurrentY;
    bool IsEnabled;
    bool IsHit;
    bool IsDud;


};

class LevelData {
public:
    LevelData();
    void loadLevel(const char* lvlFile);
public :
    float mMaxDropSpeed ;
    int mTotalPads; //multiples of columns // 3 for starters
    float mTimeToComplete;
    float mStartTime;
    std::string mPadTextureFile;
    float mPrimaryColour[4];
    float mDudColour[4];
    int mCurrentHits;
    bool mLevelPassed;
    int mTarget;
    float mPixelMoveSpeed;
};
class GamePlayScene : public Scene {
public:
    GamePlayScene(){
        mCurrentLevelIndex = 0;
        mMaxLevels = 3;
    }
    void load(const char *sceneName) override;

    void update(float deltaTime) override;

    void input(int action, float x, float y) override;

    void render(Renderer *renderer, float dt) override;
    void activate() override;
    void setup(Renderer* renderer) override;
    void LoadLevel(LevelData* data){ mCurrentLevelData = data;}
    void checkScores();
    void loadNextLevel();
    void showDialog();
    void showLoadTip();
    void loadCurrentLevel();
    void loadLevelData(int mLevel);
    void generatePads();
    void processEvent(int eventAction) override;

    float getNonDudTotalHeight() {
        float height = 0;
        for(int i=0; i < mPads.size();i++){
            if(!mPads[i]->IsDud) {
                if (mPads[i]->CurrentY < height)
                    height = mPads[i]->CurrentY;
            }
        }
        return fabs(height);
    }
    float getMinOffscreenPad(){
    float minY = 0;
    for(int i=0; i < mPads.size();i++){
        if(mPads[i]->IsEnabled) {
            if (mPads[i]->CurrentY < minY)
                minY = mPads[i]->CurrentY;
        }
    }
    return minY;
}

private:
    LevelData * mCurrentLevelData;
    Texture2D* mPadTexture;
    Sprite* mPadSprite;
    float mRunningTime;
    float mSpawnDelay;
    DialogWindow* mDialog;
    DialogWindow* mLoadTipDialog;
    std::vector<Pad*> mPads;
    int mCurrentLevelIndex;
    float mPadWidth;
    Label* btnNextLevel;
    Label* btnRetryLevel;
    Label* lbHitScore;
    Label* lbSecondsLeft;
    float mScreenHeight;
 int mMaxLevels;



};


#endif //POWERPADZ_GAMEPLAYSCENE_H

