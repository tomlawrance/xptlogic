//
// Created by toml on 18/04/2019.
//

#ifndef POWERPADZ_WELCOMESCREEN_H
#define POWERPADZ_WELCOMESCREEN_H


#include "../Engine/Scene/Scene.h"

class WelcomeScreen : public Scene {
public :
    WelcomeScreen() : Scene() {
        VisibleDuration = 2.0f ;
        mTimeVisible = 0;
    }

    void load(const char *sceneName) override;
    void update(float deltaTime) override;

    void processEvent(int eventAction) override;


private:
    float VisibleDuration;
    float mTimeVisible;
};


#endif //POWERPADZ_WELCOMESCREEN_H
