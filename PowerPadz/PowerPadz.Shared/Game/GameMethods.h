//
// Created by tomlawrance on 23/04/2019.
//

#ifndef MOBILEENGINE_GAMEMETHODS_H
#define MOBILEENGINE_GAMEMETHODS_H


#include "../Engine/Engine.h"

class GameMethods : public GameMenuMethodsBase {
public:
    void newGame() override;

    void options() override;

    void appexit() override;
};


#endif //MOBILEENGINE_GAMEMETHODS_H
