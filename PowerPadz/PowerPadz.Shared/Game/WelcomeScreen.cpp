//
// Created by toml on 18/04/2019.
//

#include <Engine/Scene/Objects/TwoD/Label.h>
#include "WelcomeScreen.h"
#include "../Engine/Graphics/Texture2D.h"
#include "../Engine/Engine.h"
#include "../Engine/Scene/Objects/TwoD/Sprite.h"

void WelcomeScreen::load(const char *sceneName) {
    Scene::load(sceneName);


    setSceneState(Scene::eSceneState::READY);
}

void WelcomeScreen::processEvent(int eventAction) {
    Scene::processEvent(eventAction);
}

void WelcomeScreen::update(float deltaTime) {
    if(mSceneState == eSceneState::READY) {
        Scene::update(deltaTime);
        mTimeVisible += deltaTime;
        if (mTimeVisible > VisibleDuration) {
            //tell something about it.
            Engine::getEngineInstance()->getSceneManager()->activateNextScene();
        }
    }
}

