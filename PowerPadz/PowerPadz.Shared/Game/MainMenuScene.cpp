//
// Created by toml on 21/04/2019.
//

#include <Engine/Engine.h>
#include <Engine/Scene/Objects/TwoD/Label.h>
#include "MainMenuScene.h"

void MainMenuScene::load(const char *sceneName) {
    Scene::load(sceneName);

    setSceneState(Scene::eSceneState::READY);

}

void MainMenuScene::processEvent(int eventAction) {
    Scene::processEvent(eventAction);
}

void MainMenuScene::update(float deltaTime) {



    if(mSceneState == eSceneState::READY) {
        Scene::update(deltaTime);

        if (firstPass) {

//            float x = (Engine::getEngineInstance()->getRenderer()->getWidth() / 2) - (mFont->measureString(lbNewGame->getText()) / 2);
//            lbNewGame->setLocation(x, 10);

            float height = Engine::getEngineInstance()->getRenderer()->getHeight();
            float totalChildHeight = 0;
            float menuStart = 0;
            for (int i = 0; i < getRoot()->getChildren().size(); i++) {
                SceneItem *item = getRoot()->getChildren()[i];

                Rect *r = item->getRenderable()->getBounds();
                totalChildHeight += r->height;
            }

            menuStart = (height / 2) - (totalChildHeight / 2);
            float accumHeight = menuStart;
            for (int i = 0; i < getRoot()->getChildren().size(); i++) {
                SceneItem *item = getRoot()->getChildren()[i];
                item->getRenderable()->setLocation(item->getRenderable()->getBounds()->x,
                                                   accumHeight);
                accumHeight += item->getRenderable()->getBounds()->height;

            }
            firstPass = false;
        }
    }
}

void MainMenuScene::input(int action, float x, float y) {
    Scene::input(action, x, y);
    for(int i =0; i < getRoot()->getChildren().size();i++) {
        SceneItem* item = getRoot()->getChildren()[i];
        Rect* r =item->getRenderable()->getBounds() ;
        if(x >= r->x && x <= (r->x+r->width)){
            if(y >= r->y && y <= (r->y +r->height)){
                //fire click action
                Engine::getEngineInstance()->Log("Menu Item Hit");
                item->getRenderable()->onClick();

            }
        }
    }
}

