//
// Created by toml on 21/04/2019.
//

#ifndef POWERPADZ_MAINMENUSCENE_H
#define POWERPADZ_MAINMENUSCENE_H


#include <Engine/Scene/Scene.h>
#include <Engine/Managers/Font/FontManager.h>
#include <Engine/Scene/Objects/TwoD/Label.h>
class MainMenuScene : public Scene{
public:
    MainMenuScene() : Scene() {
        firstPass=true;
    }

    void input(int action, float x, float y) override;

    void load(const char *sceneName) override;
    void update(float deltaTime) override;

    void processEvent(int eventAction) override;


private:
    bool firstPass;
    
   
};


#endif //POWERPADZ_MAINMENUSCENE_H
