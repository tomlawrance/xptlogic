//
// Created by toml on 03/05/2019.
//

#include <Engine/Engine.h>
#include <Engine/Managers/TextFileHelper.h>
#include "Label.h"

void Label::createVerticies() {
    if(mText.length() > 0){


//
//
//        std::vector<Line> lines;
//        std::string cWord;
//        Line ln;
//        lines.push_back(ln);
//        for(int i=0;i < mText.length();i++){
//            if((int)mText[i] == 32){
//                Word w = Word();
//                w.mWord = std::string(cWord);
//                cWord = "";
//                ln.mwords.push_back(w);
//            }
//            else{
//                cWord+=mText[i];
//            }
//        }
        vertexCount = mText.length() * 4 * 4 ;
        indexCount =  mText.length() * 6;

        float mVerticies[vertexCount];
        unsigned int mIndicies[indexCount];

        float x =0 ;
        int idx = 0;
        int vertIndex = 0;
        int vidx = 0;
        for(int i=0; i < mText.length(); i++){

            Vertex v0 = Vertex();
            Vertex v1 = Vertex();
            Vertex v2 = Vertex();
            Vertex v3 = Vertex();

            //float* uv = mFontInfo->getCharUV((int)mText[i]);
            int id = (int)mText[i];
            FontChar fc = mFontInfo->getChar(id);
            if(fc.ID == id) {

                float uv[4];
                uv[0] = fc.x / mFontInfo->scaleW;
                uv[1] = fc.y / mFontInfo->scaleH;
                uv[2] = fc.width / mFontInfo->scaleW;
                uv[3] = fc.height / mFontInfo->scaleH;

                float y = 1.0f + fc.yOffset;
                x += fc.xOffset;
                v0.x = x;
                v0.y = y;
                // v0.z = 0.0f;
                v0.ux = uv[0];
                v0.uy = uv[1];

                v1.x = x + fc.width;
                v1.y = y;
                // v1.z = 0.0f;
                v1.ux = uv[0] + uv[2];
                v1.uy = uv[1];

                v2.x = x + fc.width;
                v2.y = y + fc.height;
                // v2.z = 0.0f;
                v2.ux = uv[0] + uv[2];
                v2.uy = uv[1] + uv[3];

                v3.x = x;
                v3.y = y + fc.height;
                //v3.z = 0.0f;
                v3.ux = uv[0];
                v3.uy = uv[1] + uv[3];


                mVerticies[vidx++] = v0.x;
                mVerticies[vidx++] = v0.y;
                mVerticies[vidx++] = v0.ux;
                mVerticies[vidx++] = v0.uy;

                mVerticies[vidx++] = v1.x;
                mVerticies[vidx++] = v1.y;
                mVerticies[vidx++] = v1.ux;
                mVerticies[vidx++] = v1.uy;

                mVerticies[vidx++] = v2.x;
                mVerticies[vidx++] = v2.y;
                mVerticies[vidx++] = v2.ux;
                mVerticies[vidx++] = v2.uy;

                mVerticies[vidx++] = v3.x;
                mVerticies[vidx++] = v3.y;
                mVerticies[vidx++] = v3.ux;
                mVerticies[vidx++] = v3.uy;


                char vout[1024];
                sprintf(vout,
                        "%.2f,%.2f,%.2f,%.2f / %.2f,%.2f,%.2f,%.2f / %.2f,%.2f,%.2f,%.2f / %.2f,%.2f,%.2f,%.2f",
                        v0.x, v0.y, v0.ux, v0.uy, v1.x, v1.y, v1.ux, v1.uy, v2.x, v2.y, v2.ux,
                        v2.uy, v3.x, v3.y, v3.ux, v3.uy);
                Engine::getEngineInstance()->Log(vout);
                // 0,1,2,2,3,0
                mIndicies[idx++] = vertIndex + 0;
                mIndicies[idx++] = vertIndex + 1;
                mIndicies[idx++] = vertIndex + 2;
                mIndicies[idx++] = vertIndex + 2;
                mIndicies[idx++] = vertIndex + 3;
                mIndicies[idx++] = vertIndex + 0;

                vertIndex += 4;
                x += fc.xAdvance ;
            }
        }

        char iout [16];
        sprintf(iout, "%d:%d", idx, vidx);
        Engine::getEngineInstance()->Log(iout);

        if(mVbo == NULL)
            mVbo= new BufferObject(GL_ARRAY_BUFFER);

        mVbo->setBufferData(mVerticies, sizeof(mVerticies),GL_DYNAMIC_DRAW );

        if(mIbo == NULL)
            mIbo = new BufferObject(GL_ELEMENT_ARRAY_BUFFER);
        mIbo->setIndexBuffer(mIndicies,sizeof(mIndicies), GL_DYNAMIC_DRAW);


    }
    isDirty = false;
}

void Label::render(Renderer *renderer) {

    if(isDirty)
        createVerticies();

    if(mShader == NULL)
        setShader(ShaderProgram::getShaderProgramFromCache("sprite.vs", "sprite_tint.fs"));

    mShader->Bind();
    mVbo->Bind();
    mFontInfo->mTexture->Bind();
    mFontInfo->mTexture->Activate(GL_TEXTURE0);

    setPosition(renderer);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,4, GL_FLOAT,GL_FALSE,4 * sizeof(GL_FLOAT),(GLvoid*)0);

    glm::mat4 model = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f
    };

    model = glm::rotate(model, glm::radians(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::translate(model,glm::vec3(mLeft,mTop,0));
    model = glm::scale(model, glm::vec3(mScale,mScale,0));

    mShader->setMatrix("ProjectionMatrix", renderer->getProjectionMatrix());
    mShader->setMatrix("ModelMatrix", model);
    //if(useColour)
    float r = fgColour[1] / 255.0f;
    float g = fgColour[2]/ 255.0f;
    float b = fgColour[3]/ 255.0f;

    mShader->setFloat3("tintColour",r,g,b);
    mShader->setInt1("image", 0);

    //draw;
    mIbo->Bind();
    glDrawElements(GL_TRIANGLES,indexCount, GL_UNSIGNED_INT, (void*)0);
    mIbo->UnBind();

    mFontInfo->mTexture->UnBind();
    mVbo->UnBind();
    mShader->UnBind();
}

void Label::update(float delta) {

}

Rect *Label::getBounds() {
    Rect* r = new Rect();
    r->height = mFontInfo->lineHeight;
    r->width = mFontInfo->measureString(mText.c_str());
    r->x = mLeft;
    r->y = mTop;
    return r;
}

void Label::setSize(float width, float height) {

}

void Label::setLocation(float x, float y) {
    mLeft = x;
    mTop = y;
}

const char *Label::getText() {
    return mText.c_str();
}

void Label::load(std::vector<KVPair> keys) {
    for (int i=0; i < keys.size(); i++){
        KVPair* kv = &keys[i];
        if(kv->key.compare("text")== 0){
            setText(std::string(kv->value.c_str()));
        }else if(kv->key.compare("top")== 0){
            float top = static_cast<float>(atoi(kv->value.c_str()));
            setLocation(mLeft, top);
        }
        else if(kv->key.compare("left")== 0){
            float left = static_cast<float>(atoi(kv->value.c_str()));
            setLocation(left, mTop);
        }
        else if(kv->key.compare("hpos")== 0){
            int hpos = static_cast<int>(atoi(kv->value.c_str()));
            mHorizontalPosition = (ePosition)hpos;
            //  setLocation(left, mTop);
        }
        else if(kv->key.compare("vpos")== 0){
            int vpos = static_cast<int>(atoi(kv->value.c_str()));
            mVerticalPosition = (ePosition)vpos;
            // setLocation(left, mTop);
        }
        else if(kv->key.compare("action") == 0){
            int len = strlen(kv->value.c_str());
            mAction = new char[len];
            strcpy(mAction,kv->value.c_str() );
        }
        else if(kv->key.compare("colour") == 0){
            TextHelper::colourFromString(kv->value.c_str(), fgColour);
        }
    }
}

#define CALL_MEMBER_FN(object,ptrToMember,v)  ((object)->*(ptrToMember))(v)
void Label::onClick() {

    if(mOnClickCallback != NULL)
    {
        CALL_MEMBER_FN(tag,mOnClickCallback,callBackParam);
    }
    else {
        if (mAction != NULL && strlen(mAction) > 0)
            Engine::getEngineInstance()->runMenuCallback(mAction);
    }


}

void Label::setScale(float d) {
    mScale = d;
}

