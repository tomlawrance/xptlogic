//
// Created by toml on 03/05/2019.
//

#ifndef POWERPADZ_LABEL_H
#define POWERPADZ_LABEL_H


#include <Engine/Managers/Font/FontManager.h>
#include <Engine/Math/Vector2.h>
#include <Engine/Graphics/Vertex.h>
#include <Engine/Graphics/BufferObject.h>
#include <Engine/Graphics/ShaderProgram.h>
#include "IRenderable.h"
#include <Engine/Scene/Scene.h>
typedef void (Scene::*clickCallback)(int);

class Label : public IRenderable {
public:
    void setFont(FontInfo* fInfo){ this->mFontInfo = fInfo;}
    void setText(std::string text) {
        bool create = false;

        if (strcmp(this->mText.c_str(), text.c_str()) != 0) {
                create = true;
        }

        this->mText = std::string(text);
        if(create) {
            isDirty = true;
//            if(mVbo == NULL)
//                createVerticies(); // first pass
        }



    }
    void setLocation(float x, float y)override;
    void setShader(ShaderProgram* shader){ this->mShader = shader;}
    void setColour(float r, float g, float b){fgColour[0] = 255; fgColour[1] = r, fgColour[2] = g, fgColour[3] = b; };
    void load(std::vector<KVPair> keys)override ;
    void update(float delta) override;
    Rect *getBounds() override;
    void setSize(float width, float height) override;
    void onClick() override;
    void render(Renderer *renderer) override;
    void setOnClickCallback(clickCallback  ptr, Scene* tag, int callbackParam) { mOnClickCallback = ptr; this->tag = tag; this->callBackParam = callbackParam;}

    const char *getText();

    void setScale(float d);

private:
    FontInfo* mFontInfo;
    std::string mText;
    Vector2 mLocation;
    void createVerticies();
    BufferObject* mVbo;
    BufferObject* mIbo;
    ShaderProgram* mShader;
    unsigned int vertexCount;
    unsigned int indexCount;
    bool isDirty;
    clickCallback mOnClickCallback;
    Scene* tag;
    int callBackParam;
    char* mAction;
    float fgColour[4];
};

struct Char
{
public:
    char c;
};
struct Word{
public:
    std::string mWord;
};
struct Line{
public:
    std::vector<Word> mwords;
};



#endif //POWERPADZ_LABEL_H
