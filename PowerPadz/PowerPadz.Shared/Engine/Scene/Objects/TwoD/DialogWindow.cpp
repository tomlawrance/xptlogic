//
// Created by toml on 25/04/2019.
//

#include "DialogWindow.h"

void DialogWindow::setTexture(Texture2D *texture) {
    Sprite::setTexture(texture);
}

void DialogWindow::setLocation(float left, float top) {
    Sprite::setLocation(left, top);
}

void DialogWindow::render(Renderer *renderer) {
    Sprite::render(renderer);
    float y = mTop;
    float x= mLeft;
    if(mDialogTitle != NULL) {
        mDialogTitle->setLocation(x+16, y +20);
        mDialogTitle->setScale(0.5f);
        mDialogTitle->render(renderer);
    }
    float mnuStartY = y + 100;
    for(int i =0; i < mControls.size(); i++){
        IRenderable* sp = mControls[i];
        sp->setLocation(0, mnuStartY);

        mnuStartY+=50;
        sp->render(renderer);
    }
}

void DialogWindow::setSize(float width, float height) {
    Sprite::setSize(width, height);
}

void DialogWindow::load(std::vector<KVPair> keys) {
    Sprite::load(keys);
}

void DialogWindow::checkInput(int x, int y) {
    for(int i =0; i < mControls.size();i++){
        IRenderable* sp = mControls[i];
        if(sp->checkHit(x,y))
            sp->onClick();

    }
}
