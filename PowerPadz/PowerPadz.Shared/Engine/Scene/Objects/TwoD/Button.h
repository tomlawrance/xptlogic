//
// Created by toml on 22/04/2019.
//

#ifndef POWERPADZ_BUTTON_H
#define POWERPADZ_BUTTON_H


#include "Sprite.h"
typedef void (Scene::*clickCallback)(int);
class Button : public Sprite {
public :
    Button();

    void load(std::vector<KVPair> keys) override;
    void onClick() override ;
    void setPadding(float amount) { mPadding = amount;}
    float getPadding() { return mPadding;}

    Rect *getBounds() override;
    void setOnClickCallback(clickCallback  ptr, Scene* tag, int callbackParam) { mOnClickCallback = ptr; this->tag = tag; this->callBackParam = callbackParam;}
private:
    char* mAction;
    float mPadding;
    clickCallback mOnClickCallback;
    Scene* tag;
    int callBackParam;
};


#endif //POWERPADZ_BUTTON_H
