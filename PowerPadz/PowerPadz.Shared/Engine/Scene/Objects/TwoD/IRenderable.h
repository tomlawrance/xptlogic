//
// Created by toml on 16/04/2019.
//

#ifndef POWERPADZ_IRENDERABLE_H
#define POWERPADZ_IRENDERABLE_H

//#include <glew.h>
#ifdef __ANDROID__
#include <GLES\gl.h>
#elif __APPLE__
#include <OpenGLES/ES3/gl.h>
#endif

#include <Engine/Renderer.h>
#include <Engine/Managers/KVPair.h>
#include "Rect.h"

class IRenderable {
public:
    IRenderable() { mScale = 1.0f; mHorizontalPosition = Absolute; mVerticalPosition = Absolute;}
    enum ePosition {Absolute = 0, Near = 1, Center = 2, Far = 3};
    virtual void render(Renderer* renderer) = 0;
    virtual void update(float delta) = 0;
    virtual Rect* getBounds() =0;
    virtual void setLocation(float left, float top)=0;
    virtual void setSize(float width, float height)=0;
    virtual void onClick()=0;
    virtual void load(std::vector<KVPair> keys)=0;

    ePosition mHorizontalPosition;
    ePosition mVerticalPosition;
    float mTop;
    float mLeft;
    float mRotation;
    float mScale;

    void setPosition(Renderer* renderer) {
        Rect* r = getBounds();
        if (mHorizontalPosition != IRenderable::ePosition::Absolute) {
            switch (mHorizontalPosition) {
                case IRenderable::ePosition::Near: {
                    mLeft = 0;
                    break;
                }
                case IRenderable::ePosition::Center: {
                    mLeft = (renderer->getWidth()) / 2 - (r->width / 2);
                    break;
                }
                case IRenderable::ePosition::Far: {
                    mLeft = renderer->getWidth() - r->width;
                    break;
                }
				case IRenderable::ePosition::Absolute:
					break;
            }
        }
        if (mVerticalPosition != IRenderable::ePosition::Absolute) {
            switch (mVerticalPosition) {
                case IRenderable::ePosition::Near: {
                    mTop = 0;
                    break;
                }
                case IRenderable::ePosition::Center: {
                    mTop = (renderer->getHeight()) / 2 - (r->height / 2);
                    break;
                }
                case IRenderable::ePosition::Far: {
                    mTop = renderer->getHeight() - r->height;
                    break;
                }
				case IRenderable::ePosition::Absolute:
					break;
            }
        }
    }
    bool checkHit(int x, int y) {
        Rect* r = getBounds();
        if (y > mTop && y < mTop + r->height)
            if (x > mLeft && x < mLeft+r->width)
                return true;
        return false;
    }
};


#endif //POWERPADZ_IRENDERABLE_H
