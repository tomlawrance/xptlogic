//
// Created by toml on 12/04/2019.
//

#include <Engine/Engine.h>
#include "Sprite.h"
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

Sprite::Sprite() : Quad() {
    mLeft = 0.0f;
    mTop = 0.0f;
    mWidth = 1.0f;
    mHeight = 1.0f;
    mRotation = 0.0f;
    mRed  = 0.0f;
    mGreen = 0.0f;
    mBlue = 0.0f;

    vbo = new BufferObject(GL_ARRAY_BUFFER);
    ibo = new BufferObject(GL_ELEMENT_ARRAY_BUFFER);

    float vertices[] = {
            mCorners[0].x, mCorners[0].y,mCorners[0].ux, mCorners[0].uy,
            mCorners[1].x, mCorners[1].y,mCorners[1].ux, mCorners[1].uy,
            mCorners[2].x, mCorners[2].y,mCorners[2].ux, mCorners[2].uy,
            mCorners[3].x, mCorners[3].y,mCorners[3].ux, mCorners[3].uy,
    };
    vbo->setBufferData(vertices,sizeof(vertices), GL_STATIC_DRAW);
    ibo->setIndexBuffer(mIndex,sizeof(mIndex), GL_STATIC_DRAW);


    mShader = ShaderProgram::getShaderProgramFromCache("sprite.vs", "sprite.fs");

}

void Sprite::render(Renderer* renderer) {
    mShader->Bind();
    m_Texture->Activate(GL_TEXTURE0);
    m_Texture->Bind();

    vbo->Bind();
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,4, GL_FLOAT,GL_FALSE,4 * sizeof(GL_FLOAT),(GLvoid*)0);

    glm::mat4 model = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f
    };

    setPosition(renderer);

    model = glm::rotate(model, glm::radians(mRotation), glm::vec3(0.0f, 0.0f, 1.0f));

    model = glm::translate(model,glm::vec3(mLeft,mTop,0));
    model = glm::scale(model, glm::vec3(mWidth,mHeight,0));
    mShader->setMatrix("ProjectionMatrix", renderer->getProjectionMatrix());
    mShader->setMatrix("ModelMatrix", model);
    //if(useColour)

    float r = mRed / 255.0f;
    float g = mGreen / 255.0f;
    float b = mBlue / 255.0f;

    mShader->setFloat3("tintColour", r,g,b);
    mShader->setInt1("image", 0);
    //glDrawArrays(GL_TRIANGLES, 0, 4);
    ibo->Bind();
    glDrawElements(GL_TRIANGLE_STRIP,6,GL_UNSIGNED_INT,(void*)0);

    mShader->UnBind();
    m_Texture->Bind();
    vbo->UnBind();
    ibo->UnBind();
}

void Sprite::update(float delta) {

}

void Sprite::setSize(float width, float height) {
    mWidth = width;
    mHeight = height;
}

void Sprite::setColour(float r, float g, float b) {
    mRed = r;
    mGreen = g;
    mBlue = b;
    useColour = true;
}

void Sprite::setLocation(float left, float top) {
    mLeft = left;
    mTop = top;
}

void Sprite::load(std::vector<KVPair> keys) {
    for (int i=0; i < keys.size(); i++){
        KVPair* kv = &keys[i];
        if(kv->key.compare("filename") == 0){
            const char* fname = kv->value.c_str();
            m_Texture = Engine::getEngineInstance()->getAssetManager()->LoadTexture(fname);
            m_Texture->Upload();
        }
        else if(kv->key.compare ("width") == 0){
           float width = static_cast<float>(atoi(kv->value.c_str()));
           setSize(width, mHeight);
        }
        else if(kv->key.compare("height")== 0){
            float height = static_cast<float>(atoi(kv->value.c_str()));
            setSize(mWidth,height);
        }else if(kv->key.compare("top")== 0){
            float top = static_cast<float>(atoi(kv->value.c_str()));
            setLocation(mLeft, top);
        }
        else if(kv->key.compare("left")== 0){
            float left = static_cast<float>(atoi(kv->value.c_str()));
            setLocation(left, mTop);
        }
        else if(kv->key.compare("hpos")== 0){
            int hpos = static_cast<int>(atoi(kv->value.c_str()));
            mHorizontalPosition = (ePosition)hpos;
          //  setLocation(left, mTop);
        }
        else if(kv->key.compare("vpos")== 0){
            int vpos = static_cast<int>(atoi(kv->value.c_str()));
            mVerticalPosition = (ePosition)vpos;
           // setLocation(left, mTop);
        }
    }
}



Rect *Sprite::getBounds() {
    Rect r;
    r.x = mLeft;
    r.y = mTop;
    r.height = mHeight;
    r.width = mWidth;
    return &r;
}

void Sprite::onClick() {
   // IRenderable::onClick();
}







