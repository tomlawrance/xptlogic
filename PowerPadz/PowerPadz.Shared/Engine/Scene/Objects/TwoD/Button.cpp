//
// Created by toml on 22/04/2019.
//

#include <Engine/Engine.h>
#include <vector>
#include "Button.h"


Button::Button() {
 mPadding = 0;
 mOnClickCallback = NULL;
 mAction = NULL;
}

Rect *Button::getBounds() {
    Rect* r = Sprite::getBounds();
    r->width += (mPadding * 2);
    r->height += (mPadding * 2);

    return Sprite::getBounds();
}

void Button::load(std::vector<KVPair> keys) {

    for (int i=0; i < keys.size(); i++){
        KVPair* kv = &keys[i];
        if(kv->key.compare("action") == 0){
            int len = strlen(kv->value.c_str());
            mAction = new char[len];
            strcpy(mAction,kv->value.c_str() );


        }
        if(kv->key.compare("padding") == 0){
            float padding = static_cast<float>(atoi(kv->value.c_str()));
            setPadding(padding);


        }
    }

    Sprite::load(keys);
}
#define CALL_MEMBER_FN(object,ptrToMember,v)  ((object)->*(ptrToMember))(v)
void Button::onClick() {

    if(mOnClickCallback != NULL)
    {
        CALL_MEMBER_FN(tag,mOnClickCallback,callBackParam);
    }
    else {
        if (mAction != NULL && strlen(mAction) > 0)
            Engine::getEngineInstance()->runMenuCallback(mAction);
    }


}
