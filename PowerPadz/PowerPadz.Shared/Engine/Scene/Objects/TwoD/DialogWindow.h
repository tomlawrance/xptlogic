//
// Created by toml on 25/04/2019.
//

#ifndef POWERPADZ_DIALOGWINDOW_H
#define POWERPADZ_DIALOGWINDOW_H

#include <Engine\Engine.h>
#include "Sprite.h"
#include "Label.h"
#include <Engine/Managers/KVPair.h>
#include <Engine/Graphics/Texture2D.h>
#include <Engine/Renderer.h>


class DialogWindow : public Sprite {
public:
    DialogWindow(const char* backgroundAsset){
        Texture2D* bgTexture =Engine::getEngineInstance()->getAssetManager()->LoadTexture(backgroundAsset);
        bgTexture->Upload();
        setTexture(bgTexture);

        mDialogTitle = new Label();
        mDialogTitle->setFont(Engine::getEngineInstance()->getFontManager()->getFont("font.fnt"));
        mDialogTitle->mHorizontalPosition = IRenderable::Absolute;
        mDialogTitle->mVerticalPosition = IRenderable::Absolute;

    }
    void setTexture(Texture2D *texture) override;

    void setLocation(float left, float top) override;

    void render(Renderer *renderer) override;

    void setSize(float width, float height) override;

    void load(std::vector <KVPair> keys) override;
    void addControl(IRenderable* sprite) { mControls.push_back(sprite);}
    void clearControls(){ mControls.clear();}
    void checkInput(int x, int y);
    void setTitle (const char* title){
        if(mDialogTitle != NULL)
            mDialogTitle->setText(title);

    }
private:
    std::vector<IRenderable*> mControls;
    Label* mDialogTitle;
};


#endif //POWERPADZ_DIALOGWINDOW_H
