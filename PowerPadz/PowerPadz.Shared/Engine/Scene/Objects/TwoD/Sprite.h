//
// Created by toml on 12/04/2019.
//

#ifndef POWERPADZ_SPRITE_H
#define POWERPADZ_SPRITE_H


#include <Engine/Scene/Objects/Quad.h>
#include <Engine/Graphics/Texture2D.h>
#include <Engine/Graphics/ShaderProgram.h>
#include <Engine/Managers/KVPair.h>
#include "IRenderable.h"



//Sprite: Or TexturedQuad
class Sprite : Quad, public IRenderable{
public :

    Sprite();

    virtual void setTexture(Texture2D* texture) { this->m_Texture = texture;}
    void setColour (float r, float g , float b);
    void setLocation(float left, float top) override;
    void render(Renderer* renderer) override;
    void update(float delta) override;
    void setSize(float width, float height)override;
    void load(std::vector<KVPair> keys)override ;

    void onClick() override;


    Rect* getBounds() override ;
    void setShader(ShaderProgram* prog) { mShader = prog;}
private :
    Texture2D* m_Texture;
    BufferObject* vbo;
    BufferObject* ibo;
    ShaderProgram* mShader;
    float mWidth;
    float mHeight;


    float mRed;
    float mGreen;
    float mBlue;
    bool useColour;



};


#endif //POWERPADZ_SPRITE_H
