//
// Created by toml on 11/04/2019.
//

#ifndef POWERPADZ_QUAD_H
#define POWERPADZ_QUAD_H

#include <Engine/Math/Vector2.h>
#include <Engine/EngineTypes.h>
#include <Engine/Graphics/Vertex.h>
#include <Engine/Graphics/VertexArrayObject.h>

class Quad {
public:

    Quad(){
        mCorners[0].x = 0.0f;
        mCorners[0].y = 0.0f;
        mCorners[0].z = 0.0f;
        mCorners[0].ux = 0.0f;
        mCorners[0].uy = 0.0f;

        mCorners[1].x = 1.0f;
        mCorners[1].y = 0.0f;
        mCorners[1].z = 0.0f;
        mCorners[1].ux = 1.0f;
        mCorners[1].uy = 0.0f;

        mCorners[2].x = 1.0f;
        mCorners[2].y = 1.0f;
        mCorners[2].z = 0.0f;
        mCorners[2].ux = 1.0f;
        mCorners[2].uy = 1.0f;

        mCorners[3].x = 0.0f;
        mCorners[3].y = 1.0f;
        mCorners[3].z = 0.0f;
        mCorners[3].ux = 0.0f;
        mCorners[3].uy = 1.0f;

    };

protected:

    Vertex mCorners[4];
    uint32 mIndex[6]= { 0,1,2,2,3,0 };
private:




};


#endif //POWERPADZ_QUAD_H
