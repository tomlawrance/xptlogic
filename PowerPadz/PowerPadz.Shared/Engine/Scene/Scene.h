//
// Created by tomlawrance on 11/04/2019.
//

#ifndef MOBILEENGINE_SCENE_H
#define MOBILEENGINE_SCENE_H

#include <vector>
#include <Engine/Scene/Objects/TwoD/IRenderable.h>
#include <string>
#include <Engine/Managers/KVPair.h>


class SceneItem{
public:
    SceneItem(IRenderable* renderItem) { mRenderable = renderItem;}
    void addItem(SceneItem* item){ children.push_back(item);}
    IRenderable* getRenderable() { return mRenderable;}
    std::vector<SceneItem*> getChildren() { return children;}
private :
    void render();
    std::vector<SceneItem*> children;
    IRenderable* mRenderable;
};

class Scene {
public :
    Scene();
    enum eSceneState { LOADING, READY, PAUSED, DIALOG_SHOWING};
    virtual void load(const char* sceneName);
    virtual void update(float deltaTime);
    virtual void input(int action, float x, float y);
    virtual void activate();
    virtual void setup(Renderer* renderer);
    virtual void render(Renderer* renderer, float dt);
    virtual void processEvent(int eventAction);
    void addItem(SceneItem *pItem);
    void setSceneState (eSceneState state) { mSceneState = state;};
    eSceneState getSceneTate() { return mSceneState;}
    SceneItem* getRoot(){ return root;}
    static std::vector<KVPair> getContentPairs(const char* content);
protected:

    void processLoadScene(const char* content);
    eSceneState mSceneState;
private:
    void render(SceneItem* current,Renderer* renderer);
    void update(SceneItem *current,float delta);
  SceneItem* root;

};


#endif //MOBILEENGINE_SCENE_H
