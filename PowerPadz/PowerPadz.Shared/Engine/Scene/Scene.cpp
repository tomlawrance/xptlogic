//
// Created by tomlawrance on 11/04/2019.
//

#include <Engine/Renderer.h>
#include <Engine/Engine.h>
#include <Engine/Scene/Objects/TwoD/Button.h>
#include <Engine/Scene/Objects/TwoD/Sprite.h>
#include "Scene.h"
#include <Engine/Managers/TextFileHelper.h>
#include <Engine/Managers/SceneLoadItem.h>
#include <Engine/Scene/Objects/TwoD/Label.h>


Scene::Scene() {
    root = new SceneItem(nullptr);
    mSceneState = eSceneState ::LOADING;
}

void Scene::addItem(SceneItem *pItem) {
   root->addItem(pItem);
}

void Scene::update(float deltaTime) {
if(root != nullptr)
    update(root, deltaTime);
}
void Scene::update(SceneItem *current,float delta) {
    if(current != nullptr && current->getRenderable() != nullptr)
        current->getRenderable()->update(delta);

    for(int i=0; i < current->getChildren().size(); i++){
        SceneItem* child = current->getChildren()[i];
        update(child,delta);
    }
}


void Scene::render(Renderer* renderer, float dt) {
    if(root != nullptr){
        render(root, renderer);
    }
}

void Scene::render(SceneItem *current,Renderer* renderer) {
    if(current != nullptr && current->getRenderable() != nullptr)
        current->getRenderable()->render(renderer);

    for(int i=0; i < current->getChildren().size(); i++){
        SceneItem* child = current->getChildren()[i];
        render(child,renderer);
    }
}

void Scene::load(const char *sceneName) {
    const char* sceneConent = Engine::getEngineInstance()->LoadAsset(sceneName);
    processLoadScene(sceneConent);
}


void resursiveBuildScene(std::string content, int index, SceneLoadItem* container){
    char buffer[1024];
    int bindex = 0;
    while(index < strlen(content.c_str())){
        char c = content[index];
        if( c == '}'){
            //end of object, process it;

        }
        else{
            buffer[bindex] = c;
        }
        bindex++;
        index++;
    }
}

void buildobject(std::vector<std::string> lines, int currLine, SceneLoadItem* current) {

    SceneLoadItem* thisItem = new SceneLoadItem(current);
    if(current != NULL)
        current->Children.push_back(thisItem);
    while (currLine < lines.size()) {
        std::string line = lines[currLine];

        if(strstr(line.c_str(), "{") != 0){ // start of object
            buildobject(lines, currLine++, thisItem);
        }

        if (strstr(line.c_str(), "}") != 0) {
            return;
        }

        if (strstr(line.c_str(), "=") != 0) {
            std::vector<std::string> kvPair = TextHelper::split(line.c_str(), '=');
            if (kvPair.size() == 2) {

                KVPair kv ;
                kv.key = std::string(kvPair[0].c_str());
                kv.value = std::string(kvPair[1].c_str());

                thisItem->PropertyValues.push_back(kv);
            }
        }

        currLine++;
    }
}


 //#define LOAD2
void Scene::processLoadScene(const char *content) {
    Engine::getEngineInstance()->Log(content);


    std::vector<KVPair> keyValuePairs;
    std::string currentObject;
    std::vector<std::string> lines = TextHelper::toLines(content);
    bool inObject = false;


#ifdef LOAD2
     std::vector<SceneLoadItem> rootItems;
    SceneLoadItem* parent = new SceneLoadItem(NULL);

    for (int i = 0; i < lines.size(); i++) {
        std::string line = lines[i];
        if(strstr(line.c_str(), "{") != 0){ // start of object
            currentObject = std::string(line);
            buildobject(lines, i+1,parent);
            inObject=  true;
        }
        if(inObject) {
            if (strstr(line.c_str(), "=") != 0) {
                std::vector<std::string> kvPair = split(line.c_str(), '=');
                if (kvPair.size() == 2) {

                    KVPair kv ;
                    kv.key = std::string(kvPair[0].c_str());
                    kv.value = std::string(kvPair[1].c_str());

                    keyValuePairs.push_back(kv);
                }
            }
        }
        if(strstr(line.c_str(), "}") != 0){ // end of object
            inObject=  false;
            //generate object

        }
    }
#else


    for (int i = 0; i < lines.size(); i++) {
        std::string line = lines[i];
        if(strstr(line.c_str(), "{") != 0){ // start of object
            currentObject = std::string(line);

            inObject=  true;
        }
        if(inObject) {
            if (strstr(line.c_str(), "=") != 0) {
                std::vector<std::string> kvPair = TextHelper::split(line.c_str(), '=');
                if (kvPair.size() == 2) {

                    KVPair kv ;
                    kv.key = std::string(kvPair[0].c_str());
                    kv.value = std::string(kvPair[1].c_str());

                    keyValuePairs.push_back(kv);
                }
            }
        }
        if(strstr(line.c_str(), "}") != 0){ // end of object
            inObject=  false;
            //generate object


            if(strstr(currentObject.c_str(), "sprite") != 0) {
                Sprite* s = new Sprite();
                s->load(keyValuePairs);
                addItem(new SceneItem(s));
            }
            else if(strstr(currentObject.c_str(), "button") != 0) {
                Button* s = new Button();
                s->load(keyValuePairs);
                addItem(new SceneItem(s));
            }
            else if(strstr(currentObject.c_str(), "label") != 0) {
                Label* s = new Label();
                s->setFont(Engine::getEngineInstance()->getFontManager()->getFont("game_font.fnt")); // quite important its first.
                s->load(keyValuePairs);


                addItem(new SceneItem(s));
            }
            keyValuePairs.clear();//ready for next object;
        }
    }
#endif

}

void Scene::input(int action, float x, float y) {

}

void Scene::setup(Renderer *renderer) {

}

void Scene::activate() {

}
void Scene::processEvent(int eventAction) {

}

std::vector<KVPair> Scene::getContentPairs(const char *content) {
    std::vector<KVPair> keyValuePairs;
    std::string currentObject;
    std::vector<std::string> lines = TextHelper::toLines(content);
    for (int i = 0; i < lines.size(); i++) {
        std::string line = lines[i];


        if (strstr(line.c_str(), "=") != 0) {
            std::vector<std::string> kvPair = TextHelper::split(line.c_str(), '=');
            if (kvPair.size() == 2) {

                KVPair kv;
                kv.key = std::string(kvPair[0].c_str());
                kv.value = std::string(kvPair[1].c_str());

                keyValuePairs.push_back(kv);
            }
        }
    }
    return keyValuePairs;
}
