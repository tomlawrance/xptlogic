//
// Created by toml on 03/05/2019.
//
#include "TextFileHelper.h"
template<typename Out>
void TextHelper::split(const std::string& s, char delim, Out result)
{
    std::stringstream ss;
    ss.str(s);
    std::string item;

    /* Two while loops two separate on new line first */
    while (std::getline(ss, item))
    {
        std::stringstream ssLine;
        ssLine.str(item);
        std::string itemLine;

        /* Parse line and separate */
        size_t curCol = 0;
        while (std::getline(ssLine, itemLine, delim)) {

            *(result++) = itemLine;
            ++curCol;
        }
    }
}
std::vector<std::string> TextHelper::split(const std::string& s, char delim)
{
    std::vector<std::string> elems;
    TextHelper::split(s, delim, std::back_inserter(elems));
    return elems;
}
std::vector<std::string> TextHelper::toLines(const char* content)
{
    std::string s = std::string(content);
    std::stringstream ss;
    ss.str(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item)) {
        std::stringstream ssLine;
        ssLine.str(item);

        if (!item.empty() && item[item.size() - 1] == '\r')
            item.erase(item.size() - 1);
        elems.push_back(item);
    }
    return elems;
}
