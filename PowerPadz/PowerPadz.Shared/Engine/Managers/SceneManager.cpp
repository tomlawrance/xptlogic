//
// Created by toml on 21/04/2019.
//

#include "SceneManager.h"

void SceneManager::render(float dt, Renderer* renderer) {
    if(mRequestednextScene != NULL){
        mActiveScene = mRequestednextScene;
        mRequestednextScene = NULL;
        mActiveScene->setup(renderer);

    }

    if(mActiveScene != NULL){
        mActiveScene->update(dt);
        mActiveScene->render(renderer, dt);
    }
}

SceneManager::SceneManager() {
    mSceneIndex = 0;
    mRequestednextScene = NULL;
}

void SceneManager::addScene(Scene *scene, bool makeActive) {
    mScenes.push_back(scene);
    if(makeActive) {
        mActiveScene = scene;
        mSceneIndex = mScenes.size()-1;
    }
}

void SceneManager::activateNextScene() {

    mSceneIndex++; //todo bounds checking
    mRequestednextScene = mScenes[mSceneIndex];
}

void SceneManager::activateScene(Scene *next) {
    addScene(next, true);
}
