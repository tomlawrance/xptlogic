//
// Created by toml on 21/04/2019.
//

#ifndef POWERPADZ_SCENEMANAGER_H
#define POWERPADZ_SCENEMANAGER_H


#include <Engine/Scene/Scene.h>

class SceneManager {
public:
    SceneManager();
    void activateNextScene();
    void activateScene(Scene* next);
    void render(float dt, Renderer* renderer);
    void addScene(Scene* scene, bool makeActive);
    Scene* getScene(){ return mActiveScene;}
    private:
    std::vector<Scene*> mScenes;
    Scene* mActiveScene;
    uint32 mSceneIndex;
    Scene* mRequestednextScene;
};


#endif //POWERPADZ_SCENEMANAGER_H
