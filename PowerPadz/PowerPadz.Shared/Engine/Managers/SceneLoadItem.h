//
// Created by toml on 21/04/2019.
//

#ifndef POWERPADZ_SCENELOADITEM_H
#define POWERPADZ_SCENELOADITEM_H

#include "KVPair.h"
#include <vector>
class SceneLoadItem{
public:
    SceneLoadItem(SceneLoadItem* parent){ this->Parent = parent;}
    const char* Type;
    std::vector<KVPair> PropertyValues;
    std::vector<SceneLoadItem> Children;
    SceneLoadItem* Parent;
    std::string content;
};
#endif //POWERPADZ_SCENELOADITEM_H
