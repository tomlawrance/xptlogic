//
// Created by toml on 02/05/2019.
//

#include "FontManager.h"
#include <Engine/Engine.h>
#include <Engine/Managers/TextFileHelper.h>




FontInfo *FontInfo::LoadFont(std::string fontFile) {

    FontInfo* fInfo = new FontInfo();
    const char* fileData = Engine::getEngineInstance()->getAssetManager()->LoadAsset(fontFile.c_str());
    std::vector<std::string> lines = TextHelper::toLines(fileData);

    for(int i = 0; i < lines.size(); i++){

        std::string line = lines[i];
        std::stringstream lineStream;
        std::string tag ,kvpair, k ,v;


        lineStream << line;
        lineStream >> tag;
        if(tag == "common"){
            while(!lineStream.eof()) {
                lineStream >> kvpair;
                int idx = kvpair.find('=');
                k = kvpair.substr(0,idx);
                v = kvpair.substr(idx+1);
                std::stringstream conv;
                conv << v;
                if(k == "lineHeight")
                    conv >> fInfo->lineHeight;
                else if ( k == "base")
                    conv>> fInfo->base;
                else if ( k == "scaleW")
                    conv>> fInfo->scaleW;
                else if ( k == "scaleH")
                    conv>> fInfo->scaleH;
                else if ( k == "pages")
                    conv>> fInfo->pages;
                else if ( k == "packed")
                    conv>> fInfo->packed;
            }
        } else if (tag == "page"){
            while(!lineStream.eof()) {
                lineStream >> kvpair;
                int idx = kvpair.find('=');
                k = kvpair.substr(0, idx);
                v = kvpair.substr(idx + 1);
                std::stringstream conv;
                conv << v;
                if(k == "file")
                    conv >> fInfo->fontImageFile;

            }
        }
        else if(tag == "char"){
            FontChar c;
            while(!lineStream.eof()) {
                lineStream >> kvpair;
                int idx = kvpair.find('=');
                k = kvpair.substr(0, idx);
                v = kvpair.substr(idx + 1);
                std::stringstream conv;
                conv << v;
                if(k == "id")
                    conv >> c.ID;
                else if ( k == "x")
                    conv>> c.x;
                else if ( k == "y")
                    conv>> c.y;
                else if ( k == "width")
                    conv>> c.width;
                else if ( k == "height")
                    conv>> c.height;
                else if ( k == "xoffset")
                    conv>> c.xOffset;
                else if ( k == "yoffset")
                    conv>> c.yOffset;
                else if ( k == "xadvance")
                    conv>> c.xAdvance;

            }
            fInfo->chars[c.ID]=c;
        }
    }
    if(fInfo->fontImageFile.length() > 0) {
        fInfo->mTexture = Engine::getEngineInstance()->getAssetManager()->LoadTexture(fInfo->fontImageFile.c_str());
        fInfo->mTexture->Upload();
    }
    return fInfo;
}

float FontInfo::measureString(const char *str) {
    float width = 0;
    for(int i=0; i < strlen(str);i++){
        char c = str[i];
        FontChar fc = getChar((int)c);
        width+= fc.xAdvance;
    }
    return width;
}

FontChar FontInfo::getChar(int c) {
    auto search = chars.find(c);
    if (search != chars.end()) {
        return search->second;
    } else
        return FontChar();

}

void FontManager::LoadFont(const char *fontName) {
    FontInfo* font = FontInfo::LoadFont(fontName);
    mLoadedFonts[fontName] = font;
}

FontInfo *FontManager::getFont(const char *fontName) {
    return mLoadedFonts[fontName];
}
