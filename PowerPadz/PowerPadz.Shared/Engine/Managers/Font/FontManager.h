//
// Created by toml on 02/05/2019.
//

#ifndef POWERPADZ_FONTMANAGER_H
#define POWERPADZ_FONTMANAGER_H

#include <string>
#include <vector>
#include <map>
#include <Engine/Graphics/Texture2D.h>

class FontChar
{
public:
    int ID;
    float x;
    float y;
    float width;
    float height;
    float xOffset;
    float yOffset;
    float xAdvance;

};
class FontInfo{
public :
    float  lineHeight;
    float base;
    float scaleW;
    float scaleH;
    int pages;
    int packed;
    std::string fontImageFile;
    std::map<int, FontChar> chars;
    float measureString(const char* str);
    static FontInfo* LoadFont(std::string fontFile);
    Texture2D* mTexture;
    FontChar getChar(int c);

};

class FontManager {

public :
    void LoadFont(const char* fontName);
    FontInfo* getFont(const char* fontName);
private:
    std::map<std::string, FontInfo*> mLoadedFonts;
};



#endif //POWERPADZ_FONTMANAGER_H
