//
// Created by toml on 19/04/2019.
//

#ifndef POWERPADZ_TEXTFILEHELPER_H
#define POWERPADZ_TEXTFILEHELPER_H


#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <iostream>
#include <cstdlib>

class TextHelper{
public :
    template<typename Out>
    static void split(const std::string& s, char delim, Out result);
    static std::vector<std::string> split(const std::string& s, char delim);
    static std::vector<std::string> toLines(const char* content);

    static void colourFromString(const char* str, float colour[4]){
        int idx = 0;
        int cidx = 0;
        std::string currNumber = "";
        while(idx < strlen(str)){
            char c = str[idx];
            if(isdigit(c)){
                currNumber += c;
            }
            else if(c == ','){
                float n = std::atof(currNumber.c_str());
                colour[cidx] = n;
                cidx++;
                currNumber = "";
            }
            idx++;
        }
        if(currNumber.length() > 0){
            float n = std::atof(currNumber.c_str());
            colour[3] = n;//last
        }
    }
};


#endif //POWERPADZ_TEXTFILEHELPER_H
