//
// Created by toml on 20/04/2019.
//

#ifndef POWERPADZ_KVPAIR_H
#define POWERPADZ_KVPAIR_H
#include <string>
class KVPair{
public:
    std::string key;
    std::string value;

};
#endif //POWERPADZ_KVPAIR_H
