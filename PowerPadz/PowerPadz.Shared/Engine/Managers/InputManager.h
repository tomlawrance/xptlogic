//
// Created by toml on 21/04/2019.
//

#ifndef POWERPADZ_INPUTMANAGER_H
#define POWERPADZ_INPUTMANAGER_H

#include <android/input.h>


class InputManager {
public:
    enum eInputType { Down, Up};
    void mouseDown(float x, float y);
    void mouseup(float x, float y);
};


#endif //POWERPADZ_INPUTMANAGER_H
