//
// Created by toml on 10/04/2019.
//

#ifndef POWERPADZ_ASSETMANAGER_H
#define POWERPADZ_ASSETMANAGER_H

#include <Engine/Graphics/Texture2D.h>

class AssetManager {
public:
    virtual char* LoadAsset(const char* assetName) = 0;
    virtual Texture2D* LoadTexture(const char* assentName) = 0;
};
#endif //POWERPADZ_ASSETMANAGER_H
