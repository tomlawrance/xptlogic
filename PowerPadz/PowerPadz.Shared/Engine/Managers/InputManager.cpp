//
// Created by toml on 21/04/2019.
//

#include <Engine/Engine.h>
#include "InputManager.h"

void InputManager::mouseDown(float x, float y) {
    Engine::getEngineInstance()->getSceneManager()->getScene()->input(eInputType::Down, x, y);
}

void InputManager::mouseup(float x, float y) {
    Engine::getEngineInstance()->getSceneManager()->getScene()->input(eInputType::Up, x, y);
}
