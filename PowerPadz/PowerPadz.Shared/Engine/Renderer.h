//
// Created by toml on 09/04/2019.
//

#ifndef POWERPADZ_RENDERER_H
#define POWERPADZ_RENDERER_H


#include "EngineTypes.h"
//#include <glew.h>
#ifdef __ANDROID__
#include <GLES\gl.h>
#elif __APPLE__
#include <OpenGLES/ES3/gl.h>
#endif
#include <glm.hpp>
#include <ext.hpp>

class Renderer {
public:
    Renderer(void *pApp);

    void render();
    void resize(int32 width , int32 height);
    glm::mat4 getProjectionMatrix() { return projectMatrix;}
    bool checkExtension(const char* extension);
    float getWidth() { return mRenderWidth;}
    float getHeight() { return mRenderHeight;}
private:

    glm::mat4 projectMatrix;
    float mRenderWidth ;
    float mRenderHeight;



};


#endif //POWERPADZ_RENDERER_H
