//
// Created by toml on 17/04/2019.
//

#ifndef POWERPADZ_LOG_H
#define POWERPADZ_LOG_H
#include <android/log.h>
#include <GLES3/gl3.h>

#define  LOG_TAG    "TLogic:Engine"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

static void printGLString(const char *name, GLenum s) {
    const char *v = (const char *) glGetString(s);
    LOGI("GL %s = %s\n", name, v);
}
static void logMessage(const char* message){
    LOGI("MSG: %s\n", message);
}

#endif //POWERPADZ_LOG_H
