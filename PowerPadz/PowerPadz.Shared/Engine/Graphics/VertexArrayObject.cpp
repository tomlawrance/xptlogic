//
// Created by tomlawrance on 16/04/2019.
//

#include "VertexArrayObject.h"

void VertexArrayObject::Upload() {
    glGenVertexArrays(1, &m_ObjectID);
}

void VertexArrayObject::Delete() {
glDeleteVertexArrays(1, &m_ObjectID);
}

void VertexArrayObject::Bind() {
    if(m_ObjectID > 0)
        glBindVertexArray(m_ObjectID);
}

void VertexArrayObject::UnBind() {
    glBindVertexArray(0);
}

void VertexArrayObject::AttachVBO(BufferObject *vbo, GLuint vertexAttriArryIndex ) {
    vbo->Bind();
    Bind();
    glEnableVertexAttribArray(vertexAttriArryIndex);
    glVertexAttribPointer(vertexAttriArryIndex,4, GL_FLOAT,GL_FALSE,4 * sizeof(GL_FLOAT),(GLvoid*)0);
    vbo->UnBind();
    UnBind();
}

VertexArrayObject::VertexArrayObject() {


}

