//
// Created by tomlawrance on 16/04/2019.
//

#include "BufferObject.h"

void BufferObject::Upload() {

}

void BufferObject::Delete() {
    glDeleteBuffers(1, &m_ObjectID);
}

void BufferObject::Bind() {
    if(m_ObjectID > 0)
        glBindBuffer(bufferTarget,m_ObjectID);
}

void BufferObject::UnBind() {
    glBindBuffer(bufferTarget,0);
}

void BufferObject::setBufferData(GLfloat *verticies,uint32 count,  GLenum drawHint) {
    Bind();
    glBufferData(bufferTarget, count, verticies, drawHint);

}

BufferObject::BufferObject(GLenum bufferTarget) {
    this->bufferTarget = bufferTarget ;
    glGenBuffers(1, &m_ObjectID);
}

void BufferObject::setIndexBuffer(GLuint *indicies,uint32 count,  GLenum drawHint) {
    Bind();
    glBufferData(bufferTarget,  count, indicies, drawHint);
}
