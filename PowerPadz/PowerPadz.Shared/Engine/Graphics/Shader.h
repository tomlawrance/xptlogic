//
// Created by toml on 16/04/2019.
//

#ifndef POWERPADZ_SHADER_H
#define POWERPADZ_SHADER_H


#include "GPUObject.h"

class Shader : public GPUObject {
public:
    enum eShaderType { Vertex, Fragment };
    Shader(const char* source, eShaderType shaderType ){
        mShaderSource = source,
        mShaderType = shaderType;

        Upload();
    }
    void Upload() override;
    void Delete() override;
    void Bind() override;
    void UnBind() override;

private:
    eShaderType  mShaderType ;
    const char* mShaderSource;
    char shaderInfoLog[512];
   };


#endif //POWERPADZ_SHADER_H
