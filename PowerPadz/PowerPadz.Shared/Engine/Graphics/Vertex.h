//
// Created by tomlawrance on 16/04/2019.
//

#ifndef MOBILEENGINE_VERTEX_H
#define MOBILEENGINE_VERTEX_H

struct Vertex{
    float x;
    float y;
    float z;
    float r;
    float g;
    float b;
    float ux;
    float uy;
};
#endif //MOBILEENGINE_VERTEX_H
