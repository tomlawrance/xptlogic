//
// Created by tomlawrance on 16/04/2019.
//

#ifndef MOBILEENGINE_TEXTURE2D_H
#define MOBILEENGINE_TEXTURE2D_H


#include <GLES3/gl3.h>
#include "GPUObject.h"

class Texture2D : public GPUObject{
public:
    Texture2D (int width, int height, int* rawPixels);
    void Upload() override;
    void Delete() override;

    void Bind() override;
    void UnBind() override;

    void Activate(GLenum slot);

    int mWidth;
    int mHeight;
    char* mPixels;

private :
    void convertIntArrayToPixels(int* raw);

};


#endif //MOBILEENGINE_TEXTURE2D_H
