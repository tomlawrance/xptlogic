//
// Created by tomlawrance on 16/04/2019.
//

#include <cstring>
#include "Texture2D.h"

Texture2D::Texture2D(int width, int height, int *rawPixels) {
        mWidth = width;
        mHeight = height;
        convertIntArrayToPixels(rawPixels);
}

void Texture2D::Upload() {
    if(mWidth > 0 && mHeight > 0) {
        glGenTextures(1, &m_ObjectID);
        Bind();
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA,GL_UNSIGNED_BYTE, mPixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        //glGenerateMipmap(GL_TEXTURE_2D);
        UnBind();
    }
}

void Texture2D::Delete() {
    glDeleteTextures(1,&m_ObjectID);
}

void Texture2D::Bind() {
    if(m_ObjectID > 0)
        glBindTexture(GL_TEXTURE_2D, m_ObjectID);
}

void Texture2D::UnBind() {
    if(m_ObjectID > 0)
        glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture2D::Activate(GLenum slot) {
    glActiveTexture(slot);
}

void Texture2D::convertIntArrayToPixels(int *raw) {
    mPixels = new char[mWidth * mHeight * 4];
    for(int y =0; y < mHeight;y++){
        for(int x = 0; x < mWidth;x++){
            int rawPx = raw[y * mWidth + x];

            char a = (rawPx >> 24) & 255;
            char r = (rawPx >> 16) & 255;
            char g = (rawPx >> 8) & 255;
            char b = rawPx & 255;

            int index = ((y * mWidth) + x) *4;
            mPixels[index] = r;
            mPixels[index+1] = g;
            mPixels[index+2] = b;
            mPixels[index+3] = a;

        }
    }
//    memcpy(mPixels, rawPixels,sizeof(int) * (mWidth * mHeight));
}

