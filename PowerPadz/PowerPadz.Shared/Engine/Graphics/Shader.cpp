//
// Created by toml on 16/04/2019.
//

#include <Engine/Engine.h>
#include "Shader.h"

void Shader::Upload() {
    if(mShaderType == eShaderType::Vertex)
        m_ObjectID = glCreateShader(GL_VERTEX_SHADER);
    else if(mShaderType == eShaderType::Fragment)
        m_ObjectID = glCreateShader(GL_FRAGMENT_SHADER);
    //todo other types of shader
    glShaderSource(m_ObjectID,1,&mShaderSource,NULL);
    glCompileShader(m_ObjectID);

    int compileSuccess = 0;
    glGetShaderiv(m_ObjectID,GL_COMPILE_STATUS, &compileSuccess);
    if(!compileSuccess){
        glGetShaderInfoLog(m_ObjectID, 512,NULL, shaderInfoLog);
        Engine::getEngineInstance()->Log(shaderInfoLog);
    }
}

void Shader::Delete() {
    glDeleteShader(m_ObjectID);
}

void Shader::Bind() {

}

void Shader::UnBind() {

}
