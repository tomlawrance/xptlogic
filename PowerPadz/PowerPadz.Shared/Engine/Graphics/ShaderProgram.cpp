//
// Created by toml on 16/04/2019.
//

#include <Engine/Engine.h>
#include "ShaderProgram.h"

std::vector<ShaderProgram::ShaderCache*> ShaderProgram::SHADER_PROGRAM_CACHE;


void ShaderProgram::Upload() {




    int linkStatusSuccess;

    m_ObjectID = glCreateProgram();
    for(int i=0;i  < mPrograms.size();i++){
        Shader* shader= mPrograms[i];
        glAttachShader(m_ObjectID, shader->getObjectID());
    }
    glLinkProgram(m_ObjectID);
    glGetProgramiv(m_ObjectID,GL_LINK_STATUS,&linkStatusSuccess);
    if(!linkStatusSuccess){
        glGetProgramInfoLog(m_ObjectID,512,NULL,linkInfoLog);
        Engine::getEngineInstance()->Log(linkInfoLog);
    }

    for(int i=0;i  < mPrograms.size();i++){
        Shader* shader= mPrograms[i];
        shader->Delete();
    }


}

void ShaderProgram::Delete() {
    glDeleteProgram(m_ObjectID);
}

void ShaderProgram::Bind() {
    glUseProgram(m_ObjectID);
}

void ShaderProgram::UnBind() {
    glUseProgram(0);
}

void ShaderProgram::AddShader(Shader *shader) {
    mPrograms.push_back(shader);
}

ShaderProgram::ShaderProgram() {

}

void ShaderProgram::setMatrix(const char *uniformName, const glm::mat4 &matrix) {
    glUniformMatrix4fv(getLocation(uniformName),1,GL_FALSE,&matrix[0][0]);
}

GLuint ShaderProgram::getLocation(const char *uniformName) {
    GLuint loc = 0;
    if(mProgramLocations.find(uniformName) == mProgramLocations.end()){
        loc = glGetUniformLocation(m_ObjectID,uniformName);
        mProgramLocations[uniformName] = loc;
    } else
    {
        loc = mProgramLocations[uniformName];
    }
    return loc;
}

void ShaderProgram::setInt1(const char *uniformName, int value) {
    glUniform1i(getLocation(uniformName), value);
}

void ShaderProgram::setFloat3(const char *uniformName, float value, float value2, float value3) {
    glUniform3f(getLocation(uniformName), value,value2,value3);
}

ShaderProgram *ShaderProgram::getShaderProgramFromCache(const char *vert, const char *frag) {

    std::string hash = genShaderHash(vert,frag);
    ShaderProgram* prog = NULL;
  for(int i =0; i < SHADER_PROGRAM_CACHE.size();i++) {
      ShaderProgram::ShaderCache *cItem = SHADER_PROGRAM_CACHE[i];
      if (cItem->hash == hash) {
          return cItem->program;
      }
  }

    prog = new ShaderProgram();
    const char* vertexShaderSource = Engine::getEngineInstance()->LoadAsset(vert);
    const char* fragmentShaderSource = Engine::getEngineInstance()->LoadAsset(frag);

    prog->AddShader(new Shader(vertexShaderSource,Shader::eShaderType::Vertex));
    prog->AddShader(new Shader(fragmentShaderSource,Shader::eShaderType::Fragment));
    prog->Upload();


    SHADER_PROGRAM_CACHE.push_back(new ShaderProgram::ShaderCache(prog, hash));

    return prog;

}

std::string ShaderProgram::genShaderHash(const char *vert, const char *frag) {
    int vlen = strlen(vert);
    int flen = strlen(frag);
    char result [vlen+flen];
    strcpy(result,vert);
    strcat(result,frag);
    return std::string(result);

}

