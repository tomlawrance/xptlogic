//
// Created by tomlawrance on 16/04/2019.
//

#ifndef MOBILEENGINE_VERTEXARRAYOBJECT_H
#define MOBILEENGINE_VERTEXARRAYOBJECT_H
#include <vector>
#include "GPUObject.h"
#include "BufferObject.h"

class VertexArrayObject : GPUObject{
public:
    VertexArrayObject();
    void Upload() override;
    void Delete() override;
    void Bind() override;
    void UnBind() override;
    void AttachVBO(BufferObject* vbo, GLuint vertexAttriArryIndex);
private:
    std::vector<BufferObject*> VBO;
};


#endif //MOBILEENGINE_VERTEXARRAYOBJECT_H
