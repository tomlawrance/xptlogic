//
// Created by toml on 16/04/2019.
//

#ifndef POWERPADZ_SHADERPROGRAM_H
#define POWERPADZ_SHADERPROGRAM_H

#include <vector>
#include <map>
#include "GPUObject.h"
#include "Shader.h"
#include <glm.hpp>
#include <string>

class ShaderProgram : GPUObject {
public:

    struct ShaderCache{
    public:
        ShaderProgram* program;
        std::string hash;
        ShaderCache(ShaderProgram* p, std::string hash ){
            this->hash = hash;
            this->program = p;
        }
    };
    ShaderProgram();
    void AddShader(Shader* shader);
    void Upload() override;
    void Delete() override;
    void Bind() override;
    void UnBind() override;

    void setInt1(const char* uniformName, int  value);
    void setFloat1(const char* uniformName, float  value);
    void setFloat2(const char* uniformName, float  value, float value2);
    void setFloat3(const char* uniformName, float  value, float value2, float value3);
    void setMatrix(const char* uniformName,  const glm::mat4 &matrix);

    static ShaderProgram* getShaderProgramFromCache(const char *vert, const char *frag);
private :
    GLuint getLocation(const char* uniformName);
    std::vector<Shader*> mPrograms;
    char linkInfoLog[512];
    std::map<const char* , GLuint> mProgramLocations;
    static std::string genShaderHash(const char* vert, const char* frag);
    static std::vector<ShaderCache*> SHADER_PROGRAM_CACHE ;
};


#endif //POWERPADZ_SHADERPROGRAM_H
