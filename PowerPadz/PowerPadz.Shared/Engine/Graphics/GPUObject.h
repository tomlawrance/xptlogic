//
// Created by tomlawrance on 16/04/2019.
//

#ifndef MOBILEENGINE_GPUOBJECT_H
#define MOBILEENGINE_GPUOBJECT_H


//#include <glew.h>

#ifdef __ANDROID__
#include <GLES3/gl3.h>
#elif __APPLE__
#include <OpenGLES/ES3/gl.h>
#endif





class GPUObject {
public:
    GLuint getObjectID(){ return m_ObjectID;}
    virtual void Upload() = 0;
    virtual void Delete() = 0;
    virtual void Bind() = 0;
    virtual void UnBind() = 0;

protected:
    GLuint  m_ObjectID;
};
#endif //MOBILEENGINE_GPUOBJECT_H
