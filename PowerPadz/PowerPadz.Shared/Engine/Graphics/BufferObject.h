//
// Created by tomlawrance on 16/04/2019.
//

#ifndef MOBILEENGINE_VERTEXBUFFEROBJECT_H
#define MOBILEENGINE_VERTEXBUFFEROBJECT_H


#include <Engine/EngineTypes.h>
#include "GPUObject.h"

class BufferObject : GPUObject {

public:
    BufferObject(GLenum bufferTarget) ;

    void setBufferData(GLfloat *verticies, uint32 count, GLenum drawHint);
    void setIndexBuffer(GLuint* indicies, uint32 count ,GLenum drawHint);
    void Upload() override;
    void Delete() override;
    void Bind() override;
    void UnBind() override;

private:
    GLenum bufferTarget;
};


#endif //MOBILEENGINE_VERTEXBUFFEROBJECT_H
