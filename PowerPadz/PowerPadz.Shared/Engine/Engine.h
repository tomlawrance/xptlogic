//
// Created by toml on 10/04/2019.
//

#ifndef POWERPADZ_ENGINE_H
#define POWERPADZ_ENGINE_H

#include <Engine/Managers/AssetManager.h>
#include <Engine/Scene/Scene.h>
#include <Engine/Managers/SceneManager.h>
#include <Engine/Managers/InputManager.h>
#include <Engine/Scene/Objects/TwoD/Button.h>
#include "Renderer.h"
#include "EngineTypes.h"
#include "Log.h"
#include <map>
#include <Engine/Managers/Font/FontManager.h>


class GameMenuMethodsBase{
public:
    virtual void newGame()=0 ;
    virtual void options()=0 ;
    virtual void appexit()=0;
};

typedef void (GameMenuMethodsBase::*MenuFunctionPtr)(void);

typedef struct{
    const char* name;
    MenuFunctionPtr  func;
} MenuItem;

class Engine {
public:


    static Engine* getEngineInstance();
    Engine();

    void Resize(int32 width, int32 height);
    void Step();


    char* LoadAsset(const char* assetPathName);
    void SetAssetManager(AssetManager* assetManager);

    void Log(const char* message){ logMessage(message);}
    void GLLog(const char* message, GLenum e) { printGLString(message, e);}

    SceneManager* getSceneManager() { return mSceneManager;}
    InputManager* getInputManager() { return mInputManager;}
    Renderer* getRenderer() { return mRenderer;}
    AssetManager* getAssetManager() { return m_AssetManager;}
    FontManager* getFontManager() { return mFontManager;}
    void registerCallback(const char* name, MenuFunctionPtr func );
    void runMenuCallback(const char* name);
    void setGameMethods(GameMenuMethodsBase* methods);
private :
    GameMenuMethodsBase *mGameMethods;
    std::map<std::string, MenuFunctionPtr> mMenuCallbacks;
  // std::vector<MenuItem*> mMenuCallbacks;
//    script_map mMenuCallbacks;
    SceneManager* mSceneManager;
    InputManager* mInputManager;
    Renderer* mRenderer;
    AssetManager* m_AssetManager;
    FontManager* mFontManager;

    static Engine* mGameEngine;
    float mLastFrameNs;
    float StepTime();
};


#endif //POWERPADZ_ENGINE_H
