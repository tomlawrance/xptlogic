//
// Created by toml on 10/04/2019.
//

#include <EGL/egl.h>

#include "Engine.h"
Engine* Engine::mGameEngine = NULL;

Engine::Engine() {

    mRenderer = new Renderer(this);
    mSceneManager = new SceneManager();
    mInputManager = new InputManager();
    mFontManager = new FontManager();
    mLastFrameNs = 0;
}

void Engine::Resize(int32 width, int32 height){
    mRenderer->resize(width,height);
}
void Engine::Step(){

    float dt =StepTime();

    //update input
    //update camera
    //updates scene
    mRenderer->render();
    mSceneManager->render(dt,mRenderer);
}

char * Engine::LoadAsset(const char *assetPathName) {
 return m_AssetManager->LoadAsset(assetPathName);
}

void Engine::SetAssetManager(AssetManager *assetManager) {
    m_AssetManager = assetManager;
}

Engine *Engine::getEngineInstance() {
    if(mGameEngine == NULL)
        mGameEngine = new Engine();
    return mGameEngine;
}
#define CALL_MEMBER_FN(object,ptrToMember)  ((object)->*(ptrToMember))()
void Engine::runMenuCallback(const char *name) {

    auto sf = mMenuCallbacks.find(std::string(name));
    if(sf != mMenuCallbacks.end()){
        MenuFunctionPtr meth = sf->second;
        CALL_MEMBER_FN(mGameMethods,meth);
    }
}

void Engine::registerCallback(const char *name, MenuFunctionPtr func) {
mMenuCallbacks[name] = func;
}

void Engine::setGameMethods(GameMenuMethodsBase* methods) {
    this->mGameMethods = methods;
    registerCallback("NewGame", &GameMenuMethodsBase::newGame);
    registerCallback("Options", &GameMenuMethodsBase::options);
    registerCallback("Exit",  &GameMenuMethodsBase::appexit);

}

float Engine::StepTime() {
    /*timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    auto nowNs = now.tv_sec*1000000000ull + now.tv_nsec;
    float dt = 0;
    if (mLastFrameNs > 0) {
        dt = float(nowNs - mLastFrameNs) * 0.000000001f;

    }
    mLastFrameNs = nowNs;*/

    return 1.0f / 60.0f;
   // return dt;

}



