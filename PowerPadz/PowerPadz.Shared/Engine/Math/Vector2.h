//
// Created by toml on 11/04/2019.
//

#ifndef POWERPADZ_VECTOR2_H
#define POWERPADZ_VECTOR2_H

struct Vector2{
    float x;
    float y;
    Vector2(){ this->x = 0, this->y = 0;}
    Vector2 (float x, float y){ this->x = x, this->y = y;}

};
#endif //POWERPADZ_VECTOR2_H
