//
// Created by toml on 09/04/2019.
//

#include <GLES3/gl3.h>
#include <Engine/Renderers/GL3FunctionStubs.h>
#include <string>
#include "Renderer.h"

Renderer::Renderer(void *pApp) {

}
void Renderer::render() {
    static float grey = 1.0f;

    glClearColor(grey, grey, grey, 1.0f);
    //glClearColor(0.5f, 0.5f,0.5f,0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

}

void Renderer::resize(int32 width, int32 height) {
        glViewport(0,0,width,height);
        mRenderHeight = static_cast<float>(height);
        mRenderWidth = static_cast<float>(width);
        projectMatrix = glm::ortho(0.0f,mRenderWidth,mRenderHeight,0.0f,-1.0f , 1.0f);
}

bool Renderer::checkExtension(const char* extension) {
    if (extension == NULL) return false;

    std::string extensions = std::string((char*)glGetString(GL_EXTENSIONS));
    std::string str = std::string(extension);
    str.append(" ");

    size_t pos = 0;
    if (extensions.find(extension, pos) != std::string::npos) {
        return true;
    }

    return false;
}