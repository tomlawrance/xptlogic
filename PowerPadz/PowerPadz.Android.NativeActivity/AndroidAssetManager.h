//
// Created by toml on 10/04/2019.
//


#ifndef POWERPADZ_ANDROIDASSETMANAGER_H
#define POWERPADZ_ANDROIDASSETMANAGER_H

#include <Engine/Managers/AssetManager.h>
#include <jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <Engine/Graphics/Texture2D.h>

class AndroidAssetManager : public AssetManager {
public:
    AndroidAssetManager(ANativeActivity* act){// /*JNIEnv* env, jobject engineSupport*/) {
       /* this->engineSupportObject = engineSupport;
        loadEnv = env;*/
		this->activity = act;
		this->assetManager = act->assetManager;
        init();
    }
    void init();
    char* LoadAsset(const char *assetName) override;
    Texture2D* LoadTexture(const char* assentName) override;
    void setJNIEnv(JNIEnv* env) { loadEnv = env;}
    void setEngineSupport(jobject engineSupport){ this->engineSupportObject = engineSupport;}
private:
	ANativeActivity* activity;
    AAssetManager* assetManager;
    jobject engineSupportObject;
    JNIEnv* loadEnv;

};


#endif