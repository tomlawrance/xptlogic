//
// Created by toml on 10/04/2019.
//

#include <cassert>
#include "AndroidAssetManager.h"
#include <png.h>
#include <zlib.h>

char* AndroidAssetManager::LoadAsset(const char *assetName) {
  if(assetManager) {
      AAsset *testAsset = AAssetManager_open(assetManager, assetName, AASSET_MODE_UNKNOWN);
      if (testAsset != NULL) {
          size_t assetLength = AAsset_getLength(testAsset);
          char* fileContent = new char[assetLength+1];
          AAsset_read(testAsset, fileContent, assetLength);
          fileContent[assetLength] = '\0';

          return fileContent;
      }
  }
  return NULL;
}


Texture2D* AndroidAssetManager::LoadTexture(const char* assentName){



    if(loadEnv) {
        jclass clz = loadEnv->GetObjectClass(engineSupportObject);
        jmethodID openMethod = loadEnv->GetMethodID(clz, "LoadImage","(Ljava/lang/String;)Lcom/tlogic/powerpadz/Texture2d;");
        jstring assetParam = loadEnv->NewStringUTF(assentName);
        if (openMethod != NULL) {
            jobject textureObject = loadEnv->CallObjectMethod(engineSupportObject, openMethod,assetParam);
            jclass  objectClass = loadEnv->GetObjectClass(textureObject);
            if (textureObject) {
                jint textureWidth = loadEnv->GetIntField(textureObject, loadEnv->GetFieldID(objectClass, "width", "I"));
                jint textureHeight = loadEnv->GetIntField(textureObject, loadEnv->GetFieldID(objectClass, "height", "I"));
                jobject texturePixelsObject = loadEnv->GetObjectField(textureObject, loadEnv->GetFieldID(objectClass, "pixels", "[I"));
                jintArray *pixelArray = reinterpret_cast<jintArray *>(&texturePixelsObject);
                int *pxIntArr = loadEnv->GetIntArrayElements(*pixelArray, NULL);

                Texture2D *texture = new Texture2D(textureWidth, textureHeight, pxIntArr);
                return texture;
            }
        }
    }
    return nullptr;
}

void AndroidAssetManager::init() {

    if(loadEnv){
        jclass  clz = loadEnv->GetObjectClass(engineSupportObject);
        jmethodID  openMethod = loadEnv->GetMethodID(clz,"getAssetManager","()Landroid/content/res/AssetManager;");
        jobject managerJObject = loadEnv->CallObjectMethod(engineSupportObject,openMethod);
        if(managerJObject){
            assetManager = AAssetManager_fromJava(loadEnv,managerJObject);
            assert(NULL != assetManager);
        }
    }

}
